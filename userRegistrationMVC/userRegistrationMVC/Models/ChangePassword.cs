﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace userRegistrationMVC.Models
{
    public class ChangePassword
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "OTP gereklidir")]
        public string OTP { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Şifre gereklidir")]
        [DataType(DataType.Password)]
        [MinLength(6, ErrorMessage = "Minimum 6 karakter")]
        public string Password { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Şifre Onayı gereklidir")]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Şifre Onay alanı ile Şifre alanı eşleşmelidir")]
        public string ConfirmPassword { get; set; }
    }
}